import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.Security;

public class AESCryptographer extends Cryptographer {

    private static final String AES_ALGORITHM = "AES";

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public byte[] decrypt(byte[] encryptedText, byte[] key) throws Exception {
        System.out.println("Encrypted text: " + new String(encryptedText));

        SecretKey keySecret = new SecretKeySpec(key, AES_ALGORITHM);
        System.out.println("Key: " + new String(key));

        Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, keySecret);
        byte[] decryptedText = cipher.doFinal(encryptedText);
        System.out.println("Decrypted text: " + new String(decryptedText));

        return decryptedText;
    }

    public byte[] encrypt(byte[] decryptedText, byte[] key) throws Exception {
        if (decryptedText == null || decryptedText.length == 0) {
            throw new NullPointerException("Please, specify text for encryption");
        }
        if (key == null || key.length == 0) {
            throw new NullPointerException("Please, specify key for encryption");
        }

        System.out.println("Clear text: " + new String(decryptedText));

        SecretKey keySecret = new SecretKeySpec(key, "AES");
        System.out.println("Key: " + new String(key));

        Cipher cipher = Cipher.getInstance(AES_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, keySecret);
        byte[] encryptedText = cipher.doFinal(decryptedText);
        System.out.println("Encrypted text: " + new String(encryptedText));

        return encryptedText;
    }

    public byte[] generateKey() throws NoSuchAlgorithmException {
        KeyGenerator keyGenerator = KeyGenerator.getInstance(AES_ALGORITHM);
        keyGenerator.init(128); // 192 and 256 bits may not be available
        return keyGenerator.generateKey().getEncoded();
    }

}
