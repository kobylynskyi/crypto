import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

public class CryptographerView extends JFrame {

    private JTextArea clearTextTA;
    private JTextArea keyTA;
    private JTextArea encryptedTextTA;
    private JButton decryptButton;
    private JButton encryptButton;
    private JPanel mainPanel;
    private JButton saveClearTextButton;
    private JButton loadKeyButton;
    private JButton loadEncryptedTextButton;
    private JButton loadClearTextButton;
    private JButton saveKeyButton;
    private JButton saveEncryptedTextButton;
    private JButton generateKeyButton;
    private JComboBox encryptionType;

    public CryptographerView() {
        initComponents();
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        pack();

        encryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Cryptographer cryptographer = getSelectedCryptographer();

                    if (keyTA.getText().length() == 0) {
                        keyTA.setText(Cryptographer.byteArrayToHexString(cryptographer.generateKey()));
                    }

                    byte[] encryptedText = cryptographer.encrypt(
                            clearTextTA.getText().getBytes(),
                            Cryptographer.hexStringToByteArray(keyTA.getText()));

                    encryptedTextTA.setText(Cryptographer.byteArrayToHexString(encryptedText));
                    encryptedTextTA.setToolTipText(Arrays.toString(encryptedText));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });

        decryptButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    Cryptographer cryptographer = getSelectedCryptographer();

                    byte[] decryptedText = cryptographer.decrypt(
                            Cryptographer.hexStringToByteArray(encryptedTextTA.getText()),
                            Cryptographer.hexStringToByteArray(keyTA.getText()));

                    clearTextTA.setText(new String(decryptedText));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });

        generateKeyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    Cryptographer cryptographer = getSelectedCryptographer();

                    keyTA.setText(Cryptographer.byteArrayToHexString(cryptographer.generateKey()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });

        saveClearTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile("decoded_text_" + System.currentTimeMillis() + ".txt", clearTextTA.getText().getBytes());
            }
        });

        saveKeyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile("key_" + System.currentTimeMillis() + ".key", keyTA.getText().getBytes());
            }
        });

        saveEncryptedTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveFile("encoded_text_" + System.currentTimeMillis() + ".dat", encryptedTextTA.getText().getBytes());
            }
        });

        loadClearTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    clearTextTA.setText(new String(getContentOfSelectedFile()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });

        loadKeyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    keyTA.setText(new String(getContentOfSelectedFile()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });

        loadEncryptedTextButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    encryptedTextTA.setText(new String(getContentOfSelectedFile()));
                } catch (Exception exc) {
                    JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
                    exc.printStackTrace();
                }
            }
        });
    }

    private void initComponents() {
        setResizable(false);
        setContentPane(mainPanel);
        setLocation(150, 150);

        clearTextTA.setLineWrap(true);
        clearTextTA.setWrapStyleWord(true);
        keyTA.setLineWrap(true);
        keyTA.setWrapStyleWord(true);
        encryptedTextTA.setLineWrap(true);
        encryptedTextTA.setWrapStyleWord(true);
    }

    public Cryptographer getSelectedCryptographer() {
        Cryptographer cryptographer;
        String selectedItem = encryptionType.getSelectedItem().toString();
        Algorithm algorithmType = Algorithm.valueOf(selectedItem);
        switch (algorithmType) {
            case DES:
                cryptographer = new DESCryptographer();
                break;
            case AES:
                cryptographer = new AESCryptographer();
                break;
            case IDEA:
                cryptographer = new IDEACryptographer();
                break;
            default:
                throw new IllegalArgumentException("Could not find algorithm " + selectedItem);
        }
        return cryptographer;
    }

    public void saveFile(String name, byte[] content) {
        try {
            if (content.length > 0) {
                FileDialog fileDialog = new FileDialog(CryptographerView.this, "Save File As", FileDialog.SAVE);
                fileDialog.setFile(name);
                fileDialog.setDirectory(".");
                fileDialog.setVisible(true);
                String path = fileDialog.getDirectory() + fileDialog.getFile();

                FileOutputStream fos = new FileOutputStream(path);
                fos.write(content);
                fos.close();
            }
        } catch (Exception exc) {
            JOptionPane.showMessageDialog(CryptographerView.this, exc.getMessage());
            exc.printStackTrace();
        }
    }

    public byte[] getContentOfSelectedFile() throws IOException {
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setCurrentDirectory(new File("."));
        int ret = fileChooser.showDialog(null, "Open File");
        if (ret == JFileChooser.APPROVE_OPTION) {
            return Files.readAllBytes(Paths.get(fileChooser.getSelectedFile().getPath()));
        }
        //throw new NullPointerException("File was not opened.");
        return new byte[]{};
    }


    public static void main(String[] args) {
        new CryptographerView().setVisible(true);
    }

}
