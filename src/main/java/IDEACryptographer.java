import org.apache.commons.codec.binary.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.Security;

public class IDEACryptographer extends Cryptographer {

    private static final String IDEA_ALGORITHM = "IDEA";

    static {
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }

    public byte[] decrypt(byte[] encryptedText, byte[] key) throws Exception {
        byte[] decodedValue = Base64.decodeBase64(encryptedText);

        System.out.println("Encrypted text: " + new String(decodedValue));

        SecureRandom sr = new SecureRandom(key);
        KeyGenerator kg = KeyGenerator.getInstance(IDEA_ALGORITHM);
        kg.init(sr);
        SecretKey sk = kg.generateKey();
        System.out.println("Key: " + new String(key));

        // do the decryption with that key
        Cipher cipher = Cipher.getInstance(IDEA_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, sk);
        byte[] decryptedText = cipher.doFinal(decodedValue);
        System.out.println("Decrypted text: " + new String(decryptedText));

        return decryptedText;
    }

    public byte[] encrypt(byte[] decryptedText, byte[] key) throws Exception {
        if (decryptedText == null || decryptedText.length == 0) {
            throw new NullPointerException("Please, specify text for encryption");
        }
        if (key == null || key.length == 0) {
            throw new NullPointerException("Please, specify key for encryption");
        }

        System.out.println("Clear text: " + new String(decryptedText));

        SecureRandom secureRandom = new SecureRandom(key);
        KeyGenerator kg = KeyGenerator.getInstance(IDEA_ALGORITHM);
        kg.init(secureRandom);
        SecretKey sk = kg.generateKey();
        System.out.println("Key: " + new String(key));

        Cipher cipher = Cipher.getInstance(IDEA_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, sk);

        byte[] encryptedText = cipher.doFinal(decryptedText);
        byte[] base64encodedSecretData = Base64.encodeBase64(encryptedText);
        System.out.println("Encrypted text: " + new String(base64encodedSecretData));
        return base64encodedSecretData;
    }

    public byte[] generateKey() throws NoSuchAlgorithmException {
        return KeyGenerator.getInstance(IDEA_ALGORITHM).generateKey().getEncoded();
    }

}
